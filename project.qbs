Project {
    name: "octomy/nginx"
    Product {
    
        Group{
            name: "Config"
            prefix: "nginx"
            excludeFiles: "/**/internal/**/*"
            files: [
                "/**/*.include",
                "/**/*.conf",
                "/**/*.tpl",
                "/**/*.sh",
                "/**/*.py",
                "/**/*.local",
                "/**/*.nanorc",
                "/**/*.html",
                "/**/*.txt",
                "/**/*.json",
            ]
        }
        Group{
            name: "Meta"
            prefix: "./"
            excludeFiles: "/**/internal/**/*"
            files: [
                // Project files
                "*.qbs",
                // Environment
                ".env",
                // Ignore files for docker context
                ".gitignore",
                // Ignore files for git staging
                ".dockerignore",
                // Gitlab files such as CI/CD yaml
                ".gitlab/*",
                // Changelog detailing development history of this project
                "CHANGELOG",
                // The dockerfile to build image for this project (needs to be in the root)
                "Dockerfile",
                // Make file with dependencies
                "Makefile*",
                // README generated from template
                "README.md",
                // Version source
                "VERSION",
                // License
                "LICENSE",
                // Resource files
                "resources/*",
                // Configuration files
                "config/*.yaml",
                // Docker & compose files
                "Dockerfile",
                "docker-*.yaml",
                "docker/*.yaml",
                "docker/Dockerfile*",
                // Local helpers to load virtual environment and environment variables
                "local-*.sh",
                //Entrypoint
                "entrypoint.sh",
                // Main project file for IDE
                "project.qbs",
                // Python package requirements
                "requirements/*.in",
                "requirements/*.txt",
                // Python package configuration
                "*setup.*",
                // README template
                "resources/templates/README.md",
            ]
        }
        Group{
            name: "Design"
            prefix: "design"
            excludeFiles: "/**/internal/**/*"
            files: [
                "/**/*.jpeg",
                "/**/*.json",
                "/**/*.png",
                "/**/*.svg",
            ]
        }
    }
}
