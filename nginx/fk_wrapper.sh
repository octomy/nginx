#!/bin/bash
echo "######################## STARTING ENVSUBST  #############################"
env | grep "FK_"
export ENVSUBST_DOLLAR="$"
for i in /etc/nginx/conf.d/*.conf.tpl
do
    d=$(dirname "$i")
    f=$(basename "$i")
    e=${f#*.}
    b=${f%*.$e}
    o="$b.conf"
    echo " + SUBST '$i' --> '$o'"
    #envsubst < "$i" > "$o"
    envsubst "$(perl -e 'print "\$$_" for grep /^[_a-zA-Z]\w*$/, keys %ENV')" < "$i" > "$o"
    rm "$i"
    cat "$o"
done

# Run syslog in bg
echo "######################## STARTING SYSLOG-NG #############################"
#syslog-ng -f "/etc/syslog-ng/syslog-ng.conf" -v -e -F -d
syslog-ng -f "/etc/syslog-ng/syslog-ng.conf" -v -e &
sleep 1
#ps aux | grep syslog-ng
echo "######################## STARTING BG STATS ##############################"
function bg_stats(){
    while true
    do
        echo ""
        echo "START: BG STATS -----"
        ps aux
        echo "END:   BG STATS -----"
        echo ""
        sleep 15
    done
}

#bg_stats &

echo "######################## SHOWING CERTS FOLDER ###########################"

find /etc/letsencrypt/
echo "######################## DO LOOKUPS #####################################"

dig backhome
dig shrestha
dig octomy-website
dig localhost

echo "######################## STARTING CERTBOT ###############################"
# Now hand over control to the real entrypoint
#exec /scripts/entrypoint.sh
# Print something so we know if it exited
#echo "ENTRYPOINT FINISHED WITH EXIT CODE '$?'"

# When we get killed, kill all our children
trap "exit" INT TERM
trap "kill 0" EXIT

# Source in util.sh so we can have our nice tools
. /scripts/util.sh

# first include any user configs if they've been mounted
#template_user_configs

# Immediately run auto_enable_configs so that nginx is in a runnable state
auto_enable_configs

# Start up nginx, save PID so we can reload config inside of run_certbot.sh
echo "PS 1"
sleep 1
ps aux
echo "######################## STARTING NGINX CONFIG TEST #####################"

which nginx
nginx -t

if [ "0" != "$?" ]
then
    echo "NGINX CONFIG NOT CORRECT, TERMINATING"
    sleep 10
    exit 3
fi

echo "######################## STARTING NGINX #################################"
nginx -g "daemon off;" &
export NGINX_PID=$!
echo "STARTED NGINX WITH PID=${NGINX_PID}"
echo "PS 2"
ps aux
sleep 1
if ! ps -p $NGINX_PID > /dev/null 2>&1
then
    echo "NGINX DIED, TERMINATING"
    sleep 10
    exit 4
fi

echo "PS 3"
ps aux
echo "######################## STARTING STARTUP SCRIPTS #######################"

# Lastly, run startup scripts
for f in /scripts/startup/*.sh; do
    if [ -x "$f" ]; then
        echo "Running startup script $f"
        $f
    fi
done
echo "Done with startup"


echo "GOTCHA" > "/etc/letsencrypt/GOTCHA"

echo "######################## STARTING CERTBOT SLEEP LOOP ####################"
# Instead of trying to run `cron` or something like that, just sleep and run `certbot`.
while [ true ]; do
    echo "Run certbot"
    echo "PS 4"
    ps aux
    /scripts/run_certbot.sh
    echo "PS 5 $?"
    ps aux
    echo "Certbot finished, back to real life(tm): sleep for a week"

    # Sleep for 1 week
    sleep 604810 &
    SLEEP_PID=$!

    # Wait on sleep so that when we get ctrl-c'ed it kills everything due to our trap
    wait "$SLEEP_PID"
done


echo "SLEEP FINISHED SOMEHOW!!!"
