#!/bin/bash

function dunzip(){
	local url="$1"
	local filename="$2"
	wget "$url" -O - | gunzip > "$filename"
}

dunzip "https://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz" "GeoLite2-City.dat"
dunzip "https://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz" "GeoLite2-Country.dat"

