charset utf-8;
include       /etc/nginx/mime.types;
default_type  application/octet-stream;

include /etc/nginx/conf.d/error_codes.conf.include;


map $http_user_agent $excluded_ua {
	~LWP::Simple|BBBike|masscan  0;
	default                      1;
}


map $http_accept $triage_body {
	default "Site is UP (HTTPS)";
	~*application/json.* '{ "status": true, "message": "Site is UP (HTTPS)"}';
}


map $http_accept $triage_content_type {
	default "text/plain";
	~*application/json.* "application/json";
}


map $http_upgrade $connection_upgrade {
	default upgrade;
	'' close;
}


#resolver 127.0.0.11 [::1]:5353 valid=15s;
resolver 127.0.0.11;


server {
	server_name backhome.no;
	listen 0.0.0.0:443 ssl http2 reuseport;
	
	ssl_certificate         /etc/letsencrypt/live/backhome.no/fullchain.pem;
	ssl_certificate_key     /etc/letsencrypt/live/backhome.no/privkey.pem;
	ssl_trusted_certificate /etc/letsencrypt/live/backhome.no/chain.pem;

	# ssl_dhparam /etc/letsencrypt/dhparams/dhparam.pem;
	
	include /etc/nginx/conf.d/error_page.conf.include;
	include /etc/nginx/conf.d/deny.conf.include;
	# include /etc/nginx/conf.d/logging.conf.include;

	location /triage {
		add_header Content-Type $triage_content_type;
		return 200 $triage_body;
	}

	location / {
		# Trick to avoid nginx aborting at startup (set server in variable)
		set $upstream_server http://backhome.:8008;
		proxy_pass $upstream_server;
		
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-NginX-Proxy true;
		proxy_ssl_session_reuse off;
		proxy_set_header Host $http_host;
		proxy_cache_bypass $http_upgrade;
		proxy_redirect off;
	}
}



server {
	server_name shrestha.no;
	listen 0.0.0.0:443 ssl http2;
	
	ssl_certificate         /etc/letsencrypt/live/shrestha.no/fullchain.pem;
	ssl_certificate_key     /etc/letsencrypt/live/shrestha.no/privkey.pem;
	ssl_trusted_certificate /etc/letsencrypt/live/shrestha.no/chain.pem;
	
	# ssl_dhparam /etc/letsencrypt/dhparams/dhparam.pem;
	
	include /etc/nginx/conf.d/error_page.conf.include;
	include /etc/nginx/conf.d/deny.conf.include;
	include /etc/nginx/conf.d/logging.conf.include;
	
	location /triage {
		add_header Content-Type $triage_content_type;
		return 200 $triage_body;
	}
	
	location / {
		# Trick to avoid nginx aborting at startup (set server in variable)
		set $upstream_server http://shrestha.:8001;
		proxy_pass $upstream_server;
		
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-NginX-Proxy true;
		proxy_ssl_session_reuse off;
		proxy_set_header Host $http_host;
		proxy_cache_bypass $http_upgrade;
		proxy_redirect off;
	}
}





server {
	server_name test.octomy.org;
	listen 0.0.0.0:443 ssl http2;
	
	ssl_certificate         /etc/letsencrypt/live/test.octomy.org/fullchain.pem;
	ssl_certificate_key     /etc/letsencrypt/live/test.octomy.org/privkey.pem;
	ssl_trusted_certificate /etc/letsencrypt/live/test.octomy.org/chain.pem;
	
	# ssl_dhparam /etc/letsencrypt/dhparams/dhparam.pem;
	
	include /etc/nginx/conf.d/error_page.conf.include;
	include /etc/nginx/conf.d/deny.conf.include;
	include /etc/nginx/conf.d/logging.conf.include;
	
	location /triage {
		add_header Content-Type $triage_content_type;
		return 200 $triage_body;
	}
	
	location / {
		# Trick to avoid nginx aborting at startup (set server in variable)
		set $upstream_server http://octomy-website.:8888;
		proxy_pass $upstream_server;
		
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-NginX-Proxy true;
		proxy_ssl_session_reuse off;
		proxy_set_header Host $http_host;
		proxy_cache_bypass $http_upgrade;
		proxy_redirect off;
	}
}



server {
	server_name octomy.org;
	listen 0.0.0.0:443 ssl http2;
	
	ssl_certificate         /etc/letsencrypt/live/octomy.org/fullchain.pem;
	ssl_certificate_key     /etc/letsencrypt/live/octomy.org/privkey.pem;
	ssl_trusted_certificate /etc/letsencrypt/live/octomy.org/chain.pem;
	
	# ssl_dhparam /etc/letsencrypt/dhparams/dhparam.pem;
	
	include /etc/nginx/conf.d/error_page.conf.include;
	include /etc/nginx/conf.d/deny.conf.include;
	include /etc/nginx/conf.d/logging.conf.include;
	
	location /triage {
		add_header Content-Type $triage_content_type;
		return 200 $triage_body;
	}
	
	location / {
		# Trick to avoid nginx aborting at startup (set server in variable)
		set $upstream_server http://octomy-website.:8888;
		proxy_pass $upstream_server;
		
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-NginX-Proxy true;
		proxy_ssl_session_reuse off;
		proxy_set_header Host $http_host;
		proxy_cache_bypass $http_upgrade;
		proxy_redirect off;
	}
}

