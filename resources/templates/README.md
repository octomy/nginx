[![pipeline  status](https://gitlab.com/${FK_PROJECT_GROUP_BASE_NAME_D}/${FK_PROJECT_BASE_NAME_D}/badges/production/pipeline.svg)](https://gitlab.com/${FK_PROJECT_GROUP_BASE_NAME_D}/${FK_PROJECT_BASE_NAME_D}/-/commits/production)

${FK_TEMPLATE_WARNING}

#  Practical details
<img  src="https://gitlab.com/${FK_PROJECT_GROUP_BASE_NAME_D}/${FK_PROJECT_BASE_NAME_D}/-/raw/development/resources/images/logos/1024px.svg?ref_type=heads&amp;inline=false"  width="20%"/>

This is the ${FK_PROJECT_BASE_NAME_D} project version ${FK_VERSION_STRING}

- The website should be [live here](https://octomy.org).
- ${FK_PROJECT_BASE_NAME_D} is [available  on  gitlab](https://gitlab.com/${FK_PROJECT_GROUP_BASE_NAME_D}/${FK_PROJECT_BASE_NAME_D}).
- ${FK_PROJECT_BASE_NAME_D} is [available as private Docker image](https://gitlab.com/${FK_PROJECT_GROUP_BASE_NAME_D}/${FK_PROJECT_BASE_NAME_D}/container_registry).


```shell
#  Clone  git  repository
git  clone  git@gitlab.com:${FK_PROJECT_GROUP_BASE_NAME_D}/${FK_PROJECT_BASE_NAME_D}.git
```

```shell
# Pull image from Docker registry
docker pull registry.gitlab.com/${FK_PROJECT_GROUP_BASE_NAME_D}/${FK_PROJECT_BASE_NAME_D}

```


# What is ${FK_PROJECT_BASE_NAME_D}?

${FK_PROJECT_BASE_NAME_D} 

This is the nginx (reverse proxy) for the OctoMY&trade; website. 
