# syntax = docker/dockerfile:1.0-experimental
# https://github.com/staticfloat/docker-nginx-certbot
#FROM staticfloat/nginx-certbot@sha256:318bc3dca09a9bab4890479eca1da4db78669f97c1ba8f7ea31e996c7682a408

FROM jonasal/nginx-certbot:4.2.1-nginx1.25.0

#b930194d66dd7849ee4c2b1eff6f1ff6d58c30aed165766310ab93b8ca02e8a6

# We need syslog-ng
RUN apt-get update && \
    apt-get install -y --no-install-recommends bash diffutils syslog-ng gettext nano procps net-tools iputils-ping dnsutils && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# What is there from previously?
WORKDIR /etc/nginx/conf.d/
RUN ls -halt
RUN cat /etc/nginx/nginx.conf
#RUN cat certbot.conf

# Copy nginx configs
COPY nginx/nginx.conf /etc/nginx/
COPY nginx/config/* /etc/nginx/conf.d/

# Copy error pages
COPY nginx/error_page.html /var/www/error/

# Copy nginx maintenance scripts
COPY nginx/cgi/* /var/www/cgi/

# Make cgi socket file
#RUN mkdir -p /var/run/ \
#    && chmod 0755 /var \
#    && chmod 0755 /var/run \
#    && touch /var/run/fcgiwrap.socket \
#    && chown nginx:nginx /var/run/fcgiwrap.socket \
#    && chmod 0644 /var/run/fcgiwrap.socket \
#    && namei -l /var/run/fcgiwrap.socket

# Copy static files
# NOTE: We don't want nginx to serve static files, rather just cache static files as served by flask / uwsgi
#COPY shopify_app/static/ /etc/nginx/html/static
RUN mkdir -p /etc/nginx/html/static
RUN echo "NGINX STATIC" > /etc/nginx/html/static/nginx_static.txt
RUN ls -halt /etc/nginx/html/static
RUN du -sh /etc/nginx/html/static/*

# Copy geoip stuff
RUN mkdir -p /etc/nginx/geoip/
COPY nginx/geoip/geoip_country_code.conf /etc/nginx/geoip/

# Copy syslog-ng config
COPY nginx/syslog-ng/syslog-ng.conf /etc/syslog-ng/syslog-ng.conf
COPY nginx/fk_wrapper.sh /fk_wrapper.sh


# Show som debug info
RUN cat /etc/nginx/nginx.conf
RUN nginx -c /etc/nginx/nginx.conf -t || true
RUN syslog-ng -V
RUN syslog-ng -f "/etc/syslog-ng/syslog-ng.conf" --no-caps -v -s -e
EXPOSE 80
EXPOSE 443
EXPOSE 10443
EXPOSE 10090
# Put our syslog-ng into the loop here
CMD ["/bin/bash", "/fk_wrapper.sh"]
