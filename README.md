[![pipeline  status](https://gitlab.com/octomy/nginx/badges/production/pipeline.svg)](https://gitlab.com/octomy/nginx/-/commits/production)

<!---
                                                         
                                                         
     ## ## ## ## ## ## ## ## ## ## ##                    
        ## ## ## ## ## ## ## ## ##                       
           ## ## ## ## ## ## ##                          
              ## ## ## ## ##                             
                 ## ## ##                                
                    ##                                   
                                                         
                                                         
WARNING: This file is AUTO GENERATED from "".
         Any changes you make will be OVERWRITTEN at the 
         next invocation of relevant `make` target.      
                                                         
                                                         
                    ##                                   
                 ## ## ##                                
              ## ## ## ## ##                             
           ## ## ## ## ## ## ##                          
        ## ## ## ## ## ## ## ## ##                       
     ## ## ## ## ## ## ## ## ## ## ##                    
                                                         
                                                         
-->

#  Practical details
<img  src="https://gitlab.com/octomy/nginx/-/raw/development/resources/images/logos/1024px.svg?ref_type=heads&amp;inline=false"  width="20%"/>

This is the nginx project version 1.0.0

- The website should be [live here](https://octomy.org).
- nginx is [available  on  gitlab](https://gitlab.com/octomy/nginx).
- nginx is [available as private Docker image](https://gitlab.com/octomy/nginx/container_registry).


```shell
#  Clone  git  repository
git  clone  git@gitlab.com:octomy/nginx.git
```

```shell
# Pull image from Docker registry
docker pull registry.gitlab.com/octomy/nginx

```


# What is nginx?

nginx 

This is the nginx (reverse proxy) for the OctoMY&trade; website. 
