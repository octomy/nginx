# The project's group name in domain form (hyphen allowed, underscore not)
FK_PROJECT_GROUP_BASE_NAME_D:=octomy
# The project's base name in domain form (hyphen allowed, underscore not)
FK_PROJECT_BASE_NAME_D:=nginx

# The latest general firstkiss makefile. See 
include Makefile.fk



################# PROJECT SPESIFIC TARGETS


FK_WEBSITE_DEPLOY_DOCKERCONTEXT:=octomy-website



# We add these variables to the info target
.PHONY: info-website-deploy
info: info-website-deploy
info-website-deploy:
	@echo ""
	@echo "############### Website Deploy ##############"
	@echo ""
	@echo "FK_WEBSITE_DEPLOY_DOCKERCONTEXT:  $(FK_WEBSITE_DEPLOY_DOCKERCONTEXT)"
	@echo ""


# We add this documentation to the help target
.PHONY: help-website-deploy
help: help-website-deploy
help-website-deploy:
	@echo ""
	@echo "  Website Deploy"
	@echo ""
	@echo "    website-deploy                   - Upload current version of website to hosting"
	@echo ""


.PHONY: website-deploy
website-deploy:
	@CURRENT_CONTEXT=$$(docker context show) && \
	export DOCKER_BUILDKIT=1 && \
	export FK_DOCKER_REGISTRY_IMAGE_URL=$$(FK_DOCKER_REGISTRY_IMAGE_URL) && \
	echo "Current context is: $$CURRENT_CONTEXT" && \
	echo "Setting Docker context to $(FK_WEBSITE_DEPLOY_DOCKERCONTEXT)..." && \
	docker context use $(FK_WEBSITE_DEPLOY_DOCKERCONTEXT) && \
	echo "Performing Docker operations..." && \
	docker stack ls && \
	docker stack rm octomy-nginx && \
	docker stack ls && \
	docker stack deploy --prune --with-registry-auth --compose-file docker-stack.yaml "$(FK_PROJECT_PIP_NAME)" && \
	docker stack ls && \
	docker service logs octomy-nginx_nginx --follow && \
	echo "Reverting Docker context back to $$CURRENT_CONTEXT..." && \
	docker context use "$$CURRENT_CONTEXT"


